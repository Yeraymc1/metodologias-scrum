# Metodologias Waterfall



## Que es?

La metodologia Waterfall es un modelo sequencial utilizado para organizar las diferentes fases de un proyecto. Esto se divide en una fase de analisis, diseño, testeo y por ultimo puesta en producuion.



![alt text](https://www2.deloitte.com/content/dam/html/es/Infographics/Deloitte-ES-tecnologia-waterfall-versus-agile.jpg)




## Fases de la metodologia Waterfall

    1.Analisis
    2.Diseño
    3.Desarrollo
    4.Test y correccion de errores y ajustes finales
    5.Servicio
   
## Analisis

Todo proyecto de software comienza con una fase de análisis que incluye un estudio de viabilidad y una definición de los requisitos. En el estudio de viabilidad se evalúan los costes, la rentabilidad y la factibilidad del proyecto de software.Se desarrolla un plan y una estimación financiera del proyecto, así como una propuesta para el cliente, si fuera necesario.
A continuación, se realiza una definición detallada de los requisitos,  se define qué funciones y características debe ofrecer el producto de software para cumplir con las correspondientes exigencias.

## Diseño

En esta fase los desarrolladores se encargan de diseñar un plan detallado de la arquitectura del software. Acaba dando un borrador con el plan de diseño.

## Implementacion

En la fase de implementación se incluye la programación del software, la busqueda de errores y las pruebas unitarias. En el primer paso, el proyecto de software se traduce al correspondiente lenguaje de programación utilizada, este proceso de comprueba a través de las pruebas unitarias donde si hay algun error se puede corregir.

## Test y correccion de errores y ajustes finales

La fase de prueba se envia en una *versión de beta* y así determinar si el software cumple con las exigencias definidas del proyecto. Aquellos productos que superan la *fase beta* estan listos para su lanzamiento.

## Servicio

EL Servicio es la fase final del software en la que se autoriza y se verifica esta misma aplicacion como productiva. 
Finalmente hay que dar servicio a la entrega, mantenimiento y mejora del software.




[Pagina de donde hemos extraido la informacion.](https://www2.deloitte.com/es/es/pages/technology/articles/waterfall-vs-agile.html)

